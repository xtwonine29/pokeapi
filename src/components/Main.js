import { React, useState } from 'react';
import axios from 'axios';
import Pokemon from './Pokemon';

function Main() {

	// pomocne promenne pro loading a vypis chybovych hlasek
	const [loading, setLoading] = useState(false);
	const [error, setError] = useState(false);

	// promenne pro leveho pokemona
	const [pokeLeftName, setPokeLeftName] = useState('');
	const [pokeLeftData, setPokeLeftData] = useState(null);

	// promenne pro praveho pokemona
	const [pokeRightName, setPokeRightName] = useState('');
	const [pokeRightData, setPokeRightData] = useState(null);

	// jednoducha validace formularovych inputu
	const validateForm = () => {
		return pokeLeftName.length && pokeRightName.length;
	}

	// funkce pro vytazeni dat k zadanemu pokemonovi z API
	const getPokemonFromAPI = async (pokemonName) => {
		return new Promise((resolve, reject) => {
			axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`)
				.then((pokemonData) => {
					// v pripade uspesneho requestu se vrati ziskana data o pokemonovi skrz resolve
					resolve(pokemonData);
				})
				.catch(err => {
					// pokud request selze, vracim v rejectu implicitne NOT FOUND chybu (s jinymi
					// se v tomto domacim ukolu asi moc nesetkame)
					reject(`\r\n${pokemonName}: NOT FOUND`);
				});
		});
	}

	// funkce pro porovnani ktery pokemon ma lepsi attack
	const isWinner = (a, b) => {
		return a.stats[1].base_stat > b.stats[1].base_stat;
	}

	const handleSubmit = (e) => {
		// prevence "defaultniho odeslani" formulare
		e.preventDefault();

		// reset datovych promennych
		setPokeLeftData(null);
		setPokeRightData(null);

		// zobrazit loading, nastavit error flag na defaultni hodnotu
		setError(false);
		setLoading(true);

		// zde se provedou oba requesty "zaraz" a pak se ceka na splneni promises
		Promise.allSettled([
			getPokemonFromAPI(pokeLeftName),
			getPokemonFromAPI(pokeRightName)
		])
		// vsechny promisy jsou dobehnute
			.then(function(results) {
				// pokud jsou oba requesty fulfilled, muzeme nastavit potrebna data a zobrazit
				// pokemnony
				if(results[0].status === "fulfilled" && results[1].status === "fulfilled"){
					setPokeLeftData(results[0].value.data);
					setPokeRightData(results[1].value.data);
					setLoading(false);
				}else{
					// pokud je nejaky z requestu rejected, projedeme vsechny a u rejected
					// poukladame chybove hlasky
					let errorOutput = "";
					results.forEach((item) => {
						if(item.status === "rejected"){
							errorOutput += item.reason;
						}
					});
					// nakonec se posbirane chyby ulozi do state promenne error a zobrazi se
					setError(errorOutput);
					setLoading(false);
				}
			})
			.catch(function(err) {
				// pokud nejakym zpusobem sleti cely allSettled, zde se odchyti a vypise chyba
				setError(err);
				setLoading(false);
			});
	}
	return (
		<main>
			<div className="input">
				<form onSubmit={handleSubmit}>
					<input type="text" name="poke-left" value={pokeLeftName} onChange={(e) => setPokeLeftName(e.target.value)} />
					&nbsp; vs &nbsp;
					<input type="text" name="poke-right" value={pokeRightName} onChange={(e) => setPokeRightName(e.target.value)} />
					<br />
					<button disabled={!validateForm()}>FETCH 'EM ALL!</button>
				</form>
			</div>
			<div className={`output ${loading || error ? 'center' : ''}`}>
				{loading && <p>LOADING...</p>}
				{error && <p className="error">FETCH ERROR: <br />{error}</p> }
				{pokeLeftData && pokeRightData &&
					<>
					<Pokemon pokeData={pokeLeftData} highlight={isWinner(pokeLeftData, pokeRightData)}/>
					<Pokemon pokeData={pokeRightData} highlight={isWinner(pokeRightData, pokeLeftData)}/>
					</>
				}
			</div>
		</main>
	);
}

export default Main;
