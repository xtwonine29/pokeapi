function Pokemon(props) {

   const pokeData = props.pokeData;
   const isWinner = props.highlight;

  const sprite = pokeData.sprites.front_default;
  const name = pokeData.name;
  const hp = pokeData.stats[0].base_stat;
  const attack = pokeData.stats[1].base_stat;
  const defense = pokeData.stats[2].base_stat;
  const specialAttack = pokeData.stats[3].base_stat;
  const specialDefense = pokeData.stats[4].base_stat;
  const speed = pokeData.stats[5].base_stat;

  return (
    <div className={`pokemon ${isWinner ? 'winner' : ''}`}>
	    <img src={sprite} title={name} alt={name} />
		<h4>{name}</h4>
		<table>
			<tbody>
				<tr>
					<td>Health</td><td>{hp}</td>
				</tr>
				<tr className="attack">
					<td>Attack</td><td>{attack}</td>
				</tr>
				<tr>
					<td>Defense</td><td>{defense}</td>
				</tr>
				<tr>
					<td>Sp. Attack</td><td>{specialAttack}</td>
				</tr>
				<tr>
					<td>Sp. Defense</td><td>{specialDefense}</td>
				</tr>
				<tr>
					<td>Speed</td><td>{speed}</td>
				</tr>
			</tbody>
		</table>
    </div>
  );
}

export default Pokemon;
